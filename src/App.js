import React from 'react';
import {ServiceListComponent} from './ServiceList';

function App() {
  let servicesList = [
    {id: 1, name: "a", uptime: 0.9999},
    {id: 2, name: "ab", uptime: 0.9999},
    {id: 3, name: "ac", uptime: 0.9999},
    {id: 4, name: "ad", uptime: 0.9999},
    {id: 5, name: "az", uptime: 0.9999},
  ];

  return (
    <div>
      <ServiceListComponent services={servicesList} />
    </div>
  );
}

export default App;
