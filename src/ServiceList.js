import React from "react";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import {FormattedNumber} from "react-intl";

export function ServiceListComponent(props) {
    return (
        <CardGroup>
            {props.services.map(s => (
                <Card style={{ width: '18rem' }} key={s.id}>
                    <Card.Body>
                        <Card.Title>{s.name}</Card.Title>
                        <Card.Text>
                            <FormattedNumber
                                style={`percent`}
                                value={s.uptime}
                                minimumFractionDigits={2}
                            />
                        </Card.Text>
                    </Card.Body>
                </Card>
            ))}
        </CardGroup>
    );
}

